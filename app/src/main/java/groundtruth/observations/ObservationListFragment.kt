package groundtruth.observations

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import kotlinx.android.synthetic.main.observation_list_fragment.*
import androidx.navigation.Navigation
import ground.truth.R

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ObservationListFragment.OnListFragmentInteractionListener] interface.
 */
class ObservationListFragment : Fragment() {
    private var columnCount = 1
    private var listener: OnListFragmentInteractionListener? = null
    lateinit var view: RecyclerView
    lateinit var observationListViewModel: ObservationListViewModel
    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observationListViewModel = ObservationListViewModel.create(activity!!)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.observation_list_fragment, container, false)

        setHasOptionsMenu(true)

        observationListViewModel = ViewModelProviders.of(activity!!).get(ObservationListViewModel::class.java!!)

        val list = view.findViewById<RecyclerView>(R.id.list);

        if (list is RecyclerView) {
            this.view = list
            with(list) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MyObservationItemRecyclerViewAdapter(ObservationsContent.ITEMS, listener)
            }
        }

        observationListViewModel.loadObservations()

        observationListViewModel.lastRefresh.observe(this, Observer  { lastRefresh ->
            this.view.adapter!!.notifyDataSetChanged()
        })

        observationListViewModel.hasObservations.observe(this, Observer  { hasObservations ->
            if (!hasObservations!!){
                noObservationsHint2!!.visibility = View.VISIBLE
            } else {
                noObservationsHint2!!.visibility = View.GONE
            }
        })

        view.findViewById<View>(R.id.add_run).setOnClickListener{ it ->
           Navigation.findNavController(it).navigate(R.id.action_runListFragment_to_runFragment);
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnTimelineInteractionListener")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_lists, menu)
        this.menu = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionRefresh -> {
                observationListViewModel.loadObservations()
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun onButtonPressed(uri: Uri) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onRunClick(it: View, item: ObservationsContent.ObservationItem?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
                ObservationListFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
