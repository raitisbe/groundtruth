package groundtruth.observations

import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import ground.truth.R
import groundtruth.LocatableFragmentInteractionListener
import groundtruth.MapViewModel
import groundtruth.timeline.TimelineContent
import kotlinx.android.synthetic.main.observations_fragment.*

class ObservationsFragment : Fragment(), OnMapReadyCallback, LocatableFragmentInteractionListener {
    private lateinit var viewOfLayout:View
    lateinit var observationViewModel: ObservationViewModel
    lateinit var mapViewModel: MapViewModel
    private lateinit var navController: NavController
    lateinit var bottomSheet: FrameLayout
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var mapFragment: SupportMapFragment
    lateinit var behavior: BottomSheetBehavior<FrameLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapViewModel = MapViewModel.create(activity!!)
        observationViewModel = ObservationViewModel.create(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewOfLayout = inflater!!.inflate(R.layout.observations_fragment, container, false)

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val coordinatorLayout = viewOfLayout.findViewById<CoordinatorLayout>(R.id.container)
        val host = childFragmentManager.findFragmentById(R.id.bottom_sheet) as NavHostFragment?
        navController = host!!.navController
        bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet) as FrameLayout
        behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    alignMapToBottomsheet(mapFragment)
                }
                if (mapIsFullscreenable()) {
                    fullscreenMap(mapFragment)
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                val params = mapFragment.getView()!!.getLayoutParams()
                params.height = container!!.height - ((bottomSheet.height - 103) * slideOffset).toInt()
                mapFragment.getView()!!.setLayoutParams(params)
            }
        })

        viewOfLayout.findViewById<FloatingActionButton>(R.id.add_observation).setOnClickListener {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            observationViewModel.id = ""
            mapViewModel.currentMarker = null
        }

        behavior.peekHeight = 100
        behavior.state = BottomSheetBehavior.STATE_COLLAPSED

        return viewOfLayout
    }


    private fun createBottomSheetHeightObserver() {
        bottomSheet.getViewTreeObserver().addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                bottomSheet.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                alignMapAsNeeded()
            }
        });
    }

    fun alignMapAsNeeded() {
        if (mapIsFullscreenable())
            fullscreenMap(mapFragment)
        else
            alignMapToBottomsheet(mapFragment)
    }

    private fun mapIsFullscreenable(): Boolean {
        return arrayOf(BottomSheetBehavior.STATE_COLLAPSED, BottomSheetBehavior.STATE_HIDDEN).contains(behavior.state)
    }

    private fun alignMapToBottomsheet(mapFragment: SupportMapFragment) {
        val params = mapFragment.getView()!!.getLayoutParams()
        params.height = container.height - (bottomSheet.height - 103)
        mapFragment.getView()!!.setLayoutParams(params)
    }

    private fun fullscreenMap(mapFragment: SupportMapFragment) {
        val params = mapFragment.getView()!!.getLayoutParams()
        params.height = container.height
        mapFragment.getView()!!.setLayoutParams(params)
    }

    private fun moveToLastLocationFromPrefs() {
        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
        val latitude = sharedPref.getFloat(getString(R.string.last_latitude), 56.9727604F).toDouble()
        val longitude = sharedPref.getFloat(getString(R.string.last_longitude), 24.6124272F).toDouble()
        val zoom = sharedPref.getFloat(getString(R.string.last_zoom), 4.0F)
        moveToLocation(latitude, longitude, true, zoom)
    }



    @SuppressLint("MissingPermission")
    private fun obtieneLocalizacion() {
        if (!showPermissionDialog()) fusedClientLocate(true, {})
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        fusedClientLocate(true, {})
    }

    private fun fusedClientLocate(putStart: Boolean, listener: (() -> Unit)) {
        fusedLocationClient
                .lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        moveToLocation(location.latitude, location.longitude, putStart, 12.0F)
                    }
                    listener()
                }
    }

    fun moveToLocation(latitude: Double, longitude: Double, putStart: Boolean, zoom: Float) {
        val ll = LatLng(latitude, longitude)
        mapViewModel.lastLocation = ll
        if (putStart) mapViewModel.putStart(ll)
        mapViewModel.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, zoom))
    }

    override fun locate() {
        fusedClientLocate(false, {
            mapViewModel.locate()
        })
    }

    val locationReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            val longitude = intent?.getDoubleExtra("longitude", -1.0)
            val latitude = intent?.getDoubleExtra("latitude", -1.0)
            val time:Long? = intent?.getLongExtra("time", -1)
            val elevation= intent?.getDoubleExtra("elevation", -1.0)
            if (longitude!! > -1.0 && latitude!! > -1.0) {
                val ll = LatLng(latitude, longitude)
                mapViewModel.lastLocation = ll
                mapViewModel.appendToTrack(ll, true, elevation, time)

            }
        }
    }

    fun checkPermission(context: Context): Boolean {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private fun showPermissionDialog(): Boolean {
        if (!checkPermission(this.activity!!.applicationContext)) {
            ActivityCompat.requestPermissions(
                    this.activity!!,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                    100)
            return true
        } else return false
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mapViewModel.clearStations()
        mapViewModel.create(googleMap, navController)
        observationViewModel.parseArguments(this.arguments)
    }

    companion object {

        fun bundleArgs(obs: ObservationsContent.ObservationItem): Bundle {
            return Bundle().apply {
                this.putParcelable("observation", obs)
            }
        }

        fun bundleTimelineArgs(item: TimelineContent.TimelineItem): Bundle {
            return Bundle().apply {
                this.putParcelable("timeline", item)
            }
        }

    }
}
