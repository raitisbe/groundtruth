package groundtruth.observations

import android.app.AlertDialog
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.maps.model.LatLng
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import android.widget.Toast
import ground.truth.R
import groundtruth.*
import groundtruth.IObservationItem
import groundtruth.timeline.StartViewModel
import groundtruth.timeline.TimelineContent


class ObservationViewModel : ViewModel() {

    lateinit var mapViewModel: MapViewModel

    var deleted: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var currentObservationItem: ObservationsContent.ObservationItem
    var name: MutableLiveData<String> = MutableLiveData()
    var date: MutableLiveData<String> = MutableLiveData()
    var type: MutableLiveData<String> = MutableLiveData()
    var id:String = ""
    lateinit var activity: FragmentActivity

    fun getFullObservationInfo(id:String){
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, Config.url + "observations/" + id, null, Response.Listener { response ->
            try {
                name.value =  if (response.isNull("name")) "" else response.getString("name")
                date.value =  if (response.isNull("date")) "" else response.getString("date")
                type.value =  if (response.isNull("type")) "" else response.getString("type")
                makeScreenshotIfNeeded(response, id)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener {
            // TODO: Handle error
        })

        Requests.queue.add(jsonObjectRequest)
    }

    private fun makeScreenshotIfNeeded(response: JSONObject, id: String) {
        if (response.isNull("screenshot") || response.getString("screenshot") == "") {
            mapViewModel.makeScreenshotAndSave(mapViewModel.mMap, id, null)
        }
    }

    fun save() {
        if(id == "") return;
        val parameters = JSONObject()
        parameters.put("name", name.value.toString())
        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST,
                Config.url + "observations/" + id +"/update",
                parameters, Response.Listener { response ->
        }, Response.ErrorListener {
            Toast.makeText(activity, this.activity.getString(R.string.observationUpdateFail), Toast.LENGTH_SHORT).show()
        })

        Requests.queue.add(jsonObjectRequest)
    }

    fun fillObservations(response: JSONObject) {
        clearStations()
        val parsedStations = JSONObject(response.getString("stations"))
        val features: JSONArray = parsedStations.getJSONArray("features")
        for (i in 0..(features.length() - 1)) {
            val item = features.getJSONObject(i)
            val coords = item.getJSONObject("geometry").getJSONArray("coordinates")
            val props = item.getJSONObject("properties")
            mapViewModel.createStation(props.getInt("points"),
                    LatLng(coords.getDouble(1), coords.getDouble(0)),
                    mapViewModel.stations, props.getBoolean("visited")
            )
        }
    }

    fun parseObservation(item: IObservationItem) {
        currentObservationItem = ObservationsContent.ObservationItem(item.id, item.name, item.date, item.type, item.screenshot)
        id = item.id
        deleted.value = false
        getFullObservationInfo(item.id)
    }

    fun clearStations() {
        mapViewModel.clearStations()
    }

    fun parseArguments(arguments: Bundle?) {
        val run: ObservationsContent.ObservationItem? = arguments?.getParcelable("observation")
        val timeline: TimelineContent.TimelineItem? = arguments?.getParcelable("timeline")
        if (run != null && ::mapViewModel.isInitialized) {
            parseObservation(run)
        }
        if (timeline != null) {
            parseObservation(timeline)
        }
    }

    fun AskDelete(): AlertDialog {
        return AlertDialog.Builder(this.activity)
                //set message, title, and icon
                .setTitle(this.activity.getString(R.string.Delete))
                .setMessage(this.activity.getString(R.string.ConfirmDeleteObservation))
                .setIcon(R.drawable.ic_delete_forever_black_24dp)

                .setPositiveButton(this.activity.getString(R.string.Delete), { dialog, whichButton ->
                    delete(id)
                    dialog.dismiss()
                })

                .setNegativeButton(this.activity.getString(R.string.Cancel), { dialog, which -> dialog.dismiss() })
                .create()
    }

    private fun delete(id: String) {
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET,
                Config.url + "observations/" + id +"/remove",
                null, Response.Listener { response ->
            deleted.value = true
            val startViewModel = ViewModelProviders.of(this.activity!!)
                    .get(StartViewModel::class.java)
            val observationListViewModel = ViewModelProviders.of(this.activity!!)
                    .get(ObservationListViewModel::class.java)
            startViewModel.loadObservations()
            observationListViewModel.loadObservations()
            Toast.makeText(activity, this.activity.getString(R.string.observationDeleteSuccess), Toast.LENGTH_SHORT).show()
        }, Response.ErrorListener {
            Toast.makeText(activity, this.activity.getString(R.string.observationDeleteFail), Toast.LENGTH_SHORT).show()
        })
        Requests.queue.add(jsonObjectRequest)
    }

    companion object{
        fun create(activity: FragmentActivity): ObservationViewModel {
            var model = ViewModelProviders.of(activity).get(ObservationViewModel::class.java)
            model.activity = activity
            model.mapViewModel = ViewModelProviders.of(activity).get(MapViewModel::class.java)
            return model
        }
    }
}