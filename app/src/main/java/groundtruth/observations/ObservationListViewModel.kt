package groundtruth.observations

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import groundtruth.Config
import groundtruth.Requests
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ObservationListViewModel : ViewModel() {
    var lastRefresh: MutableLiveData<Date> = MutableLiveData()
    var hasObservations: MutableLiveData<Boolean> = MutableLiveData()
    fun loadObservations(){
        val jsonObjectRequest = JsonArrayRequest(Request.Method.GET, Config.url + "observations/list", null, Response.Listener { response ->
            try {
                ObservationsContent.ITEMS.clear()
                hasObservations.value = response.length() > 0
                for (i in 0..(response.length() - 1)) {
                    val item: JSONObject = response.getJSONObject(i)
                    ObservationsContent.ITEMS.add(ObservationsContent.ObservationItem(item.getString("_id"),
                            getName(item),
                            item.getString("date"),
                            item.getString("type"),
                            if (item.isNull("screenshot")) "" else item.getString("screenshot"))
                    )
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
           lastRefresh.value = Date()
        }, Response.ErrorListener {
            ObservationsContent.ITEMS.clear()
            lastRefresh.value = Date()
            hasObservations.value = false
        })

        Requests.queue.add(jsonObjectRequest)

    }


    private fun getName(item: JSONObject) =
            if (!item.isNull("name")) item.getString("name") else "Run"

    private fun getStations(item: JSONObject) =
            if (!item.isNull("stations")) item.getString("stations") else ""

    private fun getDistance(item: JSONObject):Double =
            if(item.isNull("distance_km")) 0.0 else item.getDouble("distance_km")

    lateinit var activity: FragmentActivity

    companion object{
        fun create(activity: FragmentActivity): ObservationListViewModel {
            var model = ViewModelProviders.of(activity).get(ObservationListViewModel::class.java)
            model.activity = activity
            return model
        }
    }
}