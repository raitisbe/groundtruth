package groundtruth.observations

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import groundtruth.observations.ObservationListFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.observation_list_item_fragment.view.*
import ground.truth.R
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class MyObservationItemRecyclerViewAdapter(
        private val mValues: List<ObservationsContent.ObservationItem>,
        private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<MyObservationItemRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ObservationsContent.ObservationItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onRunClick(v, item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.observation_list_item_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContentView.text = item.name
        val isoFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val myDate = isoFormat.parse(item.date)
        val datestyle = DateFormat.SHORT //try also MEDIUM, and FULL
        val timestyle = DateFormat.SHORT
        val humanFormat = DateFormat.getDateTimeInstance(datestyle, timestyle, Locale.getDefault());
        holder.mDateView.text = humanFormat.format(myDate)

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mDateView: TextView = mView.date
        val mContentView: TextView = mView.user

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
