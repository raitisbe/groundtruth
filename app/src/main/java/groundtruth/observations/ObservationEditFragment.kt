package groundtruth.observations

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.TextView
import kotlinx.android.synthetic.main.observation_edit_fragment.view.*

import ground.truth.R
import kotlinx.android.synthetic.main.observation_edit_fragment.*


class ObservationEditFragment : Fragment() {

    companion object {
        fun newInstance() = ObservationEditFragment()
    }

    private lateinit var viewModel: ObservationViewModel
    var distance:Double = 1.0;
    private lateinit var menu: Menu

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val viewOfLayout = inflater.inflate(R.layout.observation_edit_fragment, container, false)

        setHasOptionsMenu(true)

        viewModel = ViewModelProviders.of(activity!!).get(ObservationViewModel::class.java!!)


        viewModel.deleted.observe(this, Observer  { deleted ->
            if(::menu.isInitialized)
                menu.findItem(R.id.actionDelete).setEnabled((deleted != true))
        })

        viewModel.name.observe(this, Observer  { name ->
            if(name != viewOfLayout.name.text.toString()) {
                viewOfLayout.name.setText(name, TextView.BufferType.EDITABLE)
            }
        })

        viewOfLayout.name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(viewModel.name.value  != p0.toString()) {
                    viewModel.name.value = p0.toString()
                    viewModel.save()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        viewOfLayout.trespassing.setOnClickListener {
            markerType.setText("Trespassing")
        }

        viewOfLayout.logging.setOnClickListener {
            markerType.setText("Illegal logging")
        }

        return viewOfLayout

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.view_observation_basic, menu)
        this.menu = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionDelete -> {
                val diaBox = viewModel.AskDelete()
                diaBox.show()
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }
}
