package groundtruth.observations

import android.os.Parcel
import android.os.Parcelable
import groundtruth.IObservationItem
import java.util.ArrayList
import java.util.HashMap

object ObservationsContent {
    val ITEMS: MutableList<ObservationItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, ObservationItem> = HashMap()

    init {
    }

    private fun addItem(item: ObservationItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }

    data class ObservationItem(override val id: String, override val name: String, override val date: String, override val type: String, override val screenshot: String) : Parcelable, IObservationItem {
         constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString()) {
        }

        override fun writeToParcel(p0: Parcel?, p1: Int) {
            p0!!.writeString(this.id)
            p0!!.writeString(this.name)
            p0!!.writeString(this.date)
            p0!!.writeString(this.type)
            p0!!.writeString(this.screenshot)
        }

        override fun toString(): String = name
        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<ObservationItem> {
            override fun createFromParcel(parcel: Parcel): ObservationItem {
                return ObservationItem(parcel)
            }

            override fun newArray(size: Int): Array<ObservationItem?> {
                return arrayOfNulls(size)
            }
        }
    }
}
