package groundtruth

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.text.SpannableStringBuilder
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.navigation.NavController
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.ui.IconGenerator
import ground.truth.R
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.*


class MapViewModel : ViewModel() {
    var startPos: LatLng? = null
    var currentMarker: Marker? = null
    var stations: ArrayList<Marker> = arrayListOf<Marker>()
    lateinit var mMap: GoogleMap
    var tracker: LocationTracker = LocationTracker()
    var trackLine: Polyline? = null
    private lateinit var activity: FragmentActivity
    var lastSnapshot: Bitmap? = null
    lateinit var sharedPref: SharedPreferences
    lateinit var pointExtras: MutableList<PointExtra>

    var lastLocation: LatLng? = null

    fun startLocating() {
        tracker.startLocationButtonClick(activity)
    }

    fun getStationIconGenerator(background: Int): IconGenerator {
        val iconFactory = IconGenerator(activity)
        val dm: DisplayMetrics   = DisplayMetrics()
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        val density:Float = dm.density / 1.5F
        val multiProfile = activity.layoutInflater.inflate(R.layout.multi_profile, null)
        val mImageView = multiProfile.findViewById(R.id.image) as ImageView
        val profilePhotos: ArrayList<Drawable> = arrayListOf()
        val drawable: Drawable = ContextCompat.getDrawable(activity, background)!!
        drawable.setBounds(0, 0, (36* density).toInt(), (36* density).toInt())
        profilePhotos.add(drawable)
        val multiDrawable = MultiDrawable(profilePhotos)
        multiDrawable.setBounds(0, 0, (42* density).toInt(), (42 * density).toInt())
        mImageView.setImageDrawable(multiDrawable)
        iconFactory.setContentView(multiProfile)
        iconFactory.setBackground(null)
        return iconFactory
    }

    fun create(googleMap: GoogleMap, navController: NavController) {
        setMap(googleMap)
        mMap.setOnMapClickListener { ll: LatLng ->
            val dest = navController.currentDestination?.label
            when (dest) {
                navController.graph.findNode(R.id.observationEditFragment)!!.label -> {
                    putStart(ll)
                }
            }
        }
        mMap.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                val dest = navController.currentDestination?.label
                if (dest == "game_planning_fragment") {
                    //planningViewModel.appendToPlanningLine(marker.position)
                } else {
                    if (dest == "game_config_fragment" && marker == this@MapViewModel.currentMarker) {
                        this@MapViewModel.currentMarker = null
                        marker.remove()
                        return true
                    }
                }
                return true
            }
        })
    }

    fun putStart(ll: LatLng) {
        startPos = ll
        if (currentMarker == null) {
            val iconFactory = getStationIconGenerator(R.drawable.marker_logging)
            val iconBitmap = iconFactory.makeIcon(SpannableStringBuilder(""))
            val icon = BitmapDescriptorFactory.fromBitmap(iconBitmap)
            currentMarker = mMap.addMarker(MarkerOptions()
                    .position(ll)
                    .icon(icon)
                    .anchor(0.2F, 0.6F)
            )
        } else
            currentMarker!!.position = ll
    }

    fun appendToTrack(ll: LatLng, check_for_stations: Boolean, elevation: Double?, time: Long?) {
        if (this.trackLine == null) {
            val defaultNavMode = activity.resources.getInteger(R.integer.default_navigation_mode)
            val navMode = sharedPref.getInt(activity.getString(R.string.navigation_mode), defaultNavMode)

            this.trackLine = mMap.addPolyline(
                    PolylineOptions().add(ll)
                            .width(5.0F)
                            .color(Color.parseColor("#880271cc"))
                            .visible(navMode == 0)
                            .geodesic(true))
            pointExtras = mutableListOf()
        } else {
            val points = this.trackLine!!.points
            points.add(ll)
            this.trackLine!!.points = points
        }
        storePointExtra(elevation, time)
    }

    private fun storePointExtra(elevation: Double?, time: Long?) {
        pointExtras.add(PointExtra(elevation, time))
    }

    fun locate() {
        if (lastLocation != null)
            mMap.moveCamera(CameraUpdateFactory.newLatLng(lastLocation))
    }

    fun getTrackJson(): JSONObject {
        val tmp = JSONObject("{\"type\":\"FeatureCollection\",\"features\":[]}")
        val features = tmp.getJSONArray("features")
        val line = JSONObject("{\"type\":\"Feature\", " +
                "\"geometry\":" +
                "{\"type\":\"LineString\", " +
                "  \"coordinates\": [], " +
                "  \"properties\": {}" +
                "}}")
        if (trackLine != null) trackLine!!.points.forEachIndexed { index, latLng ->
            val pair = JSONArray()
            pair.put(latLng.longitude)
            pair.put(latLng.latitude)
            pair.put(pointExtras.get(index).elevation)
            pair.put(pointExtras.get(index).time)
            line.getJSONObject("geometry").getJSONArray("coordinates").put(pair)

        }
        features.put(line)
        return tmp
    }

    fun clearStations() {
        stations.forEach { m -> m.remove() }
        stations.clear()
    }

    fun setMap(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
        currentMarker = null
        if (startPos != null) putStart(startPos!!)
        stations.forEach { s ->
            mMap.addMarker((s.tag as ObservationTag).options)
        }
    }

    fun createStation(value: Int, randLoc: LatLng, collection: ArrayList<Marker>, visited: Boolean): Marker {
        val iconFactory = getStationIconGenerator(if (visited) R.drawable.ic_circle_green else R.drawable.ic_circle_red)
        val icon = iconFactory.makeIcon(SpannableStringBuilder(value.toString()))
        val options = MarkerOptions()
                .anchor(0.5F, 0.5F)
                .icon(BitmapDescriptorFactory.fromBitmap(icon))
                .position(randLoc)
                .title(value.toString())
        val station = mMap.addMarker(options)
        station.tag = ObservationTag(value, options)
        collection.add(station)
        return station
    }


    fun finish(listener: (() -> Unit)) {
        //Might be invisible if "difficult mode" setting is used
        if (trackLine != null) trackLine!!.isVisible = true
        mMap.snapshot({
            this.lastSnapshot = it
            listener()
        })
    }

    fun makeScreenshotAndSave(mMap: GoogleMap, id: String, callback: (() -> Unit)?) {
        mMap.setOnMapLoadedCallback {
            mMap.snapshot({
                val parameters = JSONObject()
                val byteArrayOutputStream = ByteArrayOutputStream()
                it!!.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream)
                val byteArray = byteArrayOutputStream.toByteArray()
                val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
                parameters.put("img", encoded)

                val jsonObjectRequest = JsonObjectRequest(Request.Method.POST,
                        Config.url + "observations/" + id + "/update",
                        parameters,
                        Response.Listener { response ->
                            if(callback != null) callback()
                        }, Response.ErrorListener { error ->
                    Log.e("SAVE OBSERVATION", error.toString())
                    Toast.makeText(activity, "Observation didn't save", Toast.LENGTH_SHORT).show()
                    if(callback != null) callback()
                })

                Requests.queue.add(jsonObjectRequest)
            })
        }
    }

    fun switchMapType() {
        mMap.mapType = if (mMap.mapType == GoogleMap.MAP_TYPE_TERRAIN)
            GoogleMap.MAP_TYPE_SATELLITE
        else GoogleMap.MAP_TYPE_TERRAIN
    }

    companion object {
        fun create(activity: FragmentActivity): MapViewModel {
            var mapViewModel = ViewModelProviders.of(activity).get(MapViewModel::class.java)
            mapViewModel.activity = activity
            mapViewModel.sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
            return mapViewModel
        }
    }
}



