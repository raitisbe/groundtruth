package groundtruth

import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.facebook.FacebookSdk.getApplicationContext
import java.io.ByteArrayInputStream
import java.net.CookieHandler
import java.net.CookieManager
import java.security.cert.CertificateFactory
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import com.android.volley.toolbox.HurlStack
import java.security.KeyStore
import java.security.cert.X509Certificate


object Requests {
    lateinit var queue: RequestQueue
    lateinit var fbqueue: RequestQueue

    init {

        val certificateString: String =
        "-----BEGIN CERTIFICATE-----\n" +
                "MIIDMzCCAhugAwIBAgIUdzvOcR85MQnWFOub+k0pWWleVHswDQYJKoZIhvcNAQEL\n" +
                "BQAwPTELMAkGA1UEBhMCTFYxCzAJBgNVBAgMAkxWMSEwHwYDVQQKDBhJbnRlcm5l\n" +
                "dCBXaWRnaXRzIFB0eSBMdGQwHhcNMTgxMDI1MTMzNzM3WhcNMjgwOTAyMTMzNzM3\n" +
                "WjA9MQswCQYDVQQGEwJMVjELMAkGA1UECAwCTFYxITAfBgNVBAoMGEludGVybmV0\n" +
                "IFdpZGdpdHMgUHR5IEx0ZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB\n" +
                "AL1EELj1Rd28mHrXkq2S4B6McJiwQpPIn5ozITcKY5kExOa4ePCHMMQsZIngiq+E\n" +
                "SSoCh9x5QMBeuiGb12f412cnpE/iy/1rs0M1F0F+too3w40bU414S8hYSCcW10c8\n" +
                "jjswhpCeI0wPA/NIf2bOlFsa6xuNyurWOQ5nOqc07BPJMy6zebQRA40E8vt2H4GB\n" +
                "w4azi++KtiLn3La84lVpK9e308dBxNIk113tKkPAtpEpnaHSOah0GdRJIN+Pbq0V\n" +
                "Gf6G/lbvorta00lv4ew3SMsBMhzJhZ8InGL7q8NzEzZcKuQNcsTLSLegQ86qCgtf\n" +
                "Y4bZqSZrx5DfiCTPE+4UH78CAwEAAaMrMCkwDwYDVR0RBAgwBocEwKgCZjAJBgNV\n" +
                "HRMEAjAAMAsGA1UdDwQEAwIF4DANBgkqhkiG9w0BAQsFAAOCAQEAGbLEE1qIJQhF\n" +
                "rYjtyvrhJVfLgDIPpKrE9kY/k1ENufMGmAQENoSbEJg8mnHKA+sgeeN0UKroRm+V\n" +
                "M7Gt8eH/xzgbPjBA+l1iiLvRrzPblPcc8gFeVs6O0C6+CaUcS4kM/w9YV2RrozsL\n" +
                "TicgUZWVx+puFKx3EvE9n/gt2eWx8ivdDazbVN4KwuF9rpvEobp+0N6+EwdouJnN\n" +
                "bd+C/89I8vp9k1TByPTz8ept68ELn8b30nOjnqfdxaSc2ci1i9IQWtqoy5XX7OE+\n" +
                "bYUQHRiJRiPgICrkLElBe1zw2V2THJCG2gFQyc7n2RE4qwpz0Z8htCqvQlaMfPcU\n" +
                "68J81iFd+A==\n" +
                "-----END CERTIFICATE-----\n";

        val manager = CookieManager()
        CookieHandler.setDefault(manager)

        if (Config.url.contains("192")) {
            val derInputStream = ByteArrayInputStream(certificateString.toByteArray())
            val certificateFactory = CertificateFactory.getInstance("X.509", "BC")
            val cert = certificateFactory.generateCertificate(derInputStream) as X509Certificate
            val alias = "alias"//cert.getSubjectX500Principal().getName();

            val trustStore = KeyStore.getInstance(KeyStore.getDefaultType())
            trustStore.load(null)
            trustStore.setCertificateEntry(alias, cert)
            val kmf = KeyManagerFactory.getInstance("X509")
            kmf.init(trustStore, null)
            val keyManagers = kmf.getKeyManagers()

            val tmf = TrustManagerFactory.getInstance("X509")
            tmf.init(trustStore)
            val trustManagers = tmf.getTrustManagers()

            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(keyManagers, trustManagers, null)

            val sslSF = sslContext.getSocketFactory()
            queue =  Volley.newRequestQueue(getApplicationContext(), HurlStack(null, sslSF))

        } else {
            queue =  Volley.newRequestQueue(getApplicationContext())
        }

        fbqueue =  Volley.newRequestQueue(getApplicationContext())
        println("init complete")
    }
}