package groundtruth

import android.widget.ImageView

interface IAvatarHolder {
    abstract val avatar: ImageView
}
