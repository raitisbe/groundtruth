package groundtruth.settings


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.settings_fragment.view.*

import ground.truth.R

class SettingsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.settings_fragment, container, false)

        view.saveSettings.setOnClickListener { _ ->
            save(view)
        }
        read(view)
        return view

    }

    private fun read(view: View): Boolean {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return true
        val defaultNavMode = resources.getInteger(R.integer.default_navigation_mode)
        val navMode = sharedPref.getInt(getString(R.string.navigation_mode), defaultNavMode)
        /*view.difficultMode.isChecked = navMode == 1
        view.easyMode.isChecked = navMode == 0 */
        return false
    }

    private fun save(view: View): Boolean {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return true
        with(sharedPref.edit()) {
            //putInt(getString(R.string.navigation_mode), if (view.difficultMode.isChecked()) 1 else 0)
            commit()
        }
        return false
    }


}
