package groundtruth.register


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import groundtruth.Config

import ground.truth.R
import groundtruth.Requests
import org.json.JSONException
import org.json.JSONObject
import kotlinx.android.synthetic.main.register_fragment.view.*

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.register_fragment, container, false)
        view.findViewById<View>(R.id.signUp).setOnClickListener { v ->
            if(validate(view)) {
                val parameters = JSONObject()
                parameters.put("email", view.emailText.text)
                parameters.put("password", view.passwordText.text)
                parameters.put("first", view.nameText.text)
                parameters.put("last", view.surnameText.text)

                val jsonObjectRequest: JsonObjectRequest = JsonObjectRequest(Request.Method.POST, Config.url + "auth/signup", parameters, Response.Listener { response ->
                    try {
                        if (response.getBoolean("result")) {
                            Toast.makeText(activity, "Registration successful", Toast.LENGTH_SHORT).show()
                            Navigation.findNavController(v).navigate(R.id.startFragment)
                        } else {
                            Toast.makeText(activity, "Registration unsuccessful. Reason: " + response.getString("error"), Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { it: VolleyError ->
                    it.printStackTrace()
                })

                Requests.queue.add<JSONObject?>(jsonObjectRequest)
            }
        }

        return view
    }

    fun validate(view:View):Boolean{
        if(view.passwordText.text.toString().isEmpty() || !view.passwordText.text.toString().equals(view.passwordText2.text.toString())) {
            Toast.makeText(activity, "Passwords need to match", Toast.LENGTH_SHORT).show()
            return false
        }
        if(view.emailText.text.isEmpty()) {
            Toast.makeText(activity, "Must specify an email", Toast.LENGTH_SHORT).show()
            return false
        }
        if(view.nameText.text.isEmpty()) {
            Toast.makeText(activity, "Must specify a name or nickname", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

}
