package groundtruth

/*Used to store extra information in track line points like
 timestamp when the user has been at that location */
class PointExtra(val elevation: Double?, val time: Long?)