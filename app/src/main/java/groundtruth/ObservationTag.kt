package groundtruth

import com.google.android.gms.maps.model.MarkerOptions

class ObservationTag constructor (var value: Int, val options:MarkerOptions) {
    var type:String = ""
}