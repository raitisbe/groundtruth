package groundtruth

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView

import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import ground.truth.R
import groundtruth.observations.ObservationsFragment
import groundtruth.observations.ObservationListFragment
import groundtruth.observations.ObservationsContent
import groundtruth.timeline.StartFragment
import groundtruth.timeline.TimelineContent
import kotlinx.android.synthetic.main.nav_header_test.*
import kotlinx.android.synthetic.main.start_activity.*

class StartActivity : AppCompatActivity(), ObservationListFragment.OnListFragmentInteractionListener, StartFragment.OnTimelineInteractionListener {
    lateinit var drawer: DrawerLayout
    private var securityViewModel: SecurityViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_activity)
        securityViewModel = SecurityViewModel.create(this)
        val host = supportFragmentManager.findFragmentById(R.id.my_fragment) as NavHostFragment?
        val navController = host!!.navController
        drawer = container
        val nav_view = findViewById<NavigationView>(R.id.nav_drawer)
        NavigationUI.setupActionBarWithNavController(this, navController, drawer)
        val logout = nav_view.getHeaderView(0).findViewById<ImageButton>(R.id.logout)

        securityViewModel!!.profileName.observe(this, Observer { profile ->
            profileName?.setText(profile)
        })

        securityViewModel!!.profilePic.observe(this, Observer { bitmap ->
            val profileImage = findViewById<ImageView>(R.id.profile)
            profileImage?.setImageBitmap(bitmap)
        })


        securityViewModel!!.authorized.observe(this, Observer { status ->
            logout.visibility = if (status!!) View.VISIBLE else View.INVISIBLE
        })

        NavigationUI.setupWithNavController(nav_view, navController)

        logout!!.setOnClickListener { v -> securityViewModel!!.logout() }

        securityViewModel!!.start()
    }


    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(drawer, Navigation.findNavController(this, R.id.my_fragment))
    }

    override fun onBackPressed() {
        val drawer = container as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onRunClick(it: View, item: ObservationsContent.ObservationItem?) {
        val args = ObservationsFragment.bundleArgs(item!!)
        Navigation.findNavController(it).navigate(R.id.action_runListFragment_to_runFragment, args)
    }

    override fun onTimelineClick(v: View, item: TimelineContent.TimelineItem?) {
        val args = ObservationsFragment.bundleTimelineArgs(item!!)
        Navigation.findNavController(v).navigate(R.id.action_startFragment_to_observationFragment, args)
    }


}
