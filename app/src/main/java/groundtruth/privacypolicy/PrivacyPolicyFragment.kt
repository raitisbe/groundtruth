package groundtruth.privacypolicy


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ground.truth.R
import android.webkit.WebView
import android.webkit.WebViewClient



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class privacyPolicyFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view:View = inflater.inflate(R.layout.privacypolicy_fragment, container, false)
        var webView = view.findViewById(R.id.webView1) as WebView
        webView.getSettings().setJavaScriptEnabled(true)
        webView.loadUrl("http://rogain.online/privacy_policy")

// you need to setWebViewClient for forcefully open in your webview
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        })
        return view

    }


}
