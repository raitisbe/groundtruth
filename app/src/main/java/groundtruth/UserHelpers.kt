package groundtruth

import android.graphics.Bitmap
import android.widget.ImageView
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import groundtruth.Requests
import org.json.JSONObject

class UserHelpers {

    companion object {
        fun getUsername(item: JSONObject):String{
            if(item.isNull("user")) return ""
            val user = item.getJSONObject("user")
            if(!user.isNull("name")){
                val name = user.getString("name")
                return name
            }
            return ""
        }

        fun getProfileImage(item: JSONObject):String{
            if(item.isNull("user") && item.isNull("avatar")) return ""
            var s = ""
            if(!item.isNull("user")) {
                val user = item.getJSONObject("user")
                if (!user.isNull("avatar")) {
                    s = user.getString("avatar")
                }
            } else if(!item.isNull("avatar")){
                s = item.getString("avatar")
            }
            return s
        }

        fun getProfileImage(holder: IAvatarHolder, url:String){
            val img_url = url.replace("600".toRegex(), "64")
            val request = ImageRequest(img_url,
                    Response.Listener { bitmap ->
                        holder.avatar.setImageBitmap( bitmap)
                    }, 64, 64, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.ARGB_8888,
                    Response.ErrorListener { err ->
                        err.printStackTrace()
                    })
            Requests.fbqueue.add(request)
        }
    }

}