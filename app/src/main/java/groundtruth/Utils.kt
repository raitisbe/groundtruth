package groundtruth

import com.google.android.gms.maps.model.LatLng
import java.util.*
import java.util.concurrent.TimeUnit

class Utils {

    companion object {
        const val EARTH_RADIUS = 6371 // Approx Earth radius in KM
        fun distance(pos1: LatLng, pos2: LatLng): Double {
            val dLat = Math.toRadians(pos2.latitude - pos1.latitude)
            val dLong = Math.toRadians(pos2.longitude - pos1.longitude)

            val startLat = Math.toRadians(pos1.latitude)
            val endLat = Math.toRadians(pos2.latitude)

            val a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong)
            val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

            return EARTH_RADIUS * c // <-- d
        }

        fun timeString(millis_until_finished: Long): String {
            var millisUntilFinished: Long = millis_until_finished
            val hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
            millisUntilFinished -= TimeUnit.HOURS.toMillis(hours)

            val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
            millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes)

            val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)

            // Format the string
            return String.format(
                    Locale.getDefault(),
                    "%02d:%02d:%02d",
                    hours, minutes, seconds
            )
        }


        fun haversin(v: Double): Double {
            return Math.pow(Math.sin(v / 2), 2.0)
        }
    }
}