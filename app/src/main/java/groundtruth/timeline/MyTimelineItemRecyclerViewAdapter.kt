package groundtruth.timeline

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import kotlinx.android.synthetic.main.timeline_item_fragment.view.*
import groundtruth.Config
import ground.truth.R
import groundtruth.Requests
import groundtruth.Utils
import groundtruth.IAvatarHolder
import groundtruth.UserHelpers
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class MyTimelineItemRecyclerViewAdapter(
        private val mValues: List<TimelineContent.TimelineItem>,
        private val mListener: StartFragment.OnTimelineInteractionListener?,
        val maxWidth: Int)
    : RecyclerView.Adapter<MyTimelineItemRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as TimelineContent.TimelineItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onTimelineClick(v, item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.timeline_item_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.user.text = item.user
        holder.title.text = item.name
        val df = DecimalFormat("0.00 km")
        val isoFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val myDate = isoFormat.parse(item.date)
        val datestyle = DateFormat.MEDIUM //try also MEDIUM, and FULL
        val timestyle = DateFormat.SHORT
        val humanFormat = DateFormat.getDateTimeInstance(datestyle, timestyle, Locale.getDefault());
        holder.mDateView.text = humanFormat.format(myDate)
        UserHelpers.getProfileImage(holder, item.avatar)
        getMapScreenshot(holder, item.screenshot)
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    fun getMapScreenshot(holder: ViewHolder, url:String){
        if(url != "") {
            holder.screenshot.visibility = VISIBLE
            val request = ImageRequest(Config.url + "../" + url,
                    Response.Listener { bitmap ->
                        holder.screenshot.setImageBitmap(bitmap)
                    }, maxWidth, (maxWidth * 0.75).toInt(), ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                    Response.ErrorListener { err ->
                        err.printStackTrace()
                    })
            Requests.queue.add(request)
        } else {
            holder.screenshot.visibility = GONE
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView), IAvatarHolder {
        val mDateView: TextView = mView.date
        override val avatar: ImageView = mView.avatar
        val screenshot: ImageView = mView.screenshot
        val user: TextView = mView.user
        val title: TextView = mView.title

        override fun toString(): String {
            return super.toString() + " '" + user.text + "'"
        }
    }
}

