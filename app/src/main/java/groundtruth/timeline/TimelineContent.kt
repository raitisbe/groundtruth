package groundtruth.timeline

import android.os.Parcel
import android.os.Parcelable
import groundtruth.IObservationItem
import java.util.ArrayList
import java.util.HashMap

object TimelineContent {
    val ITEMS: MutableList<TimelineItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, TimelineItem> = HashMap()

    init {
    }

    private fun addItem(item: TimelineItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }

    data class TimelineItem(override val id: String, override val name: String, override val date: String, override val type: String, val user: String, val avatar: String, override val screenshot: String) : Parcelable, IObservationItem {
        constructor(parcel: Parcel) : this(
                parcel.readString(), //id
                parcel.readString(), // name
                parcel.readString(), // date
                parcel.readString(), //type
                parcel.readString(), //user
                parcel.readString(), //avatar
                parcel.readString() //screenshot
                ) {
        }

        override fun writeToParcel(p0: Parcel?, p1: Int) {
            p0!!.writeString(this.id)
            p0!!.writeString(this.name)
            p0!!.writeString(this.date)
            p0!!.writeString(this.type)
            p0!!.writeString(this.user)
            p0!!.writeString(this.avatar)
            p0!!.writeString(this.screenshot)
        }

        override fun toString(): String = name
        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<TimelineItem> {
            override fun createFromParcel(parcel: Parcel): TimelineItem {
                return TimelineItem(parcel)
            }

            override fun newArray(size: Int): Array<TimelineItem?> {
                return arrayOfNulls(size)
            }
        }
    }
}
