package groundtruth.timeline

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.*
import android.widget.ImageView
import android.widget.RelativeLayout

import com.facebook.login.widget.LoginButton

import java.util.Arrays

import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.login_layout.view.*
import ground.truth.R
import groundtruth.SecurityViewModel

class StartFragment : Fragment() {

    private var mViewModel: StartViewModel? = null
    private var loginButton: LoginButton? = null
    private var loginFrame: RelativeLayout? = null
    private var imageView: ImageView? = null
    lateinit var securityViewModel: SecurityViewModel
    lateinit var startViewModel: StartViewModel
    private var listener: OnTimelineInteractionListener? = null
    private lateinit var menu: Menu

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.timeline_fragment, container, false)

        setHasOptionsMenu(true)

        view.findViewById<View>(R.id.add_run2).setOnClickListener { v -> Navigation.findNavController(v).navigate(R.id.action_startFragment_to_observationFragment) }
        view.findViewById<View>(R.id.signUp).setOnClickListener { v -> Navigation.findNavController(v).navigate(R.id.action_startFragment_to_registerFragment) }

        loginFrame = view.findViewById<View>(R.id.loginFrame) as RelativeLayout
        imageView = view.findViewById(R.id.image)

        loginButton = view.findViewById<View>(R.id.login_button) as LoginButton
        loginButton!!.fragment = this
        loginButton!!.setReadPermissions(Arrays.asList(EMAIL))

        securityViewModel = ViewModelProviders.of(this.activity!!).get(SecurityViewModel::class.java)

        startViewModel = ViewModelProviders.of(this.activity!!).get(StartViewModel::class.java)

        val list = view.findViewById<RecyclerView>(R.id.list)

        /*
        securityViewModel!!.authorized.observe(this, Observer { authorized ->
            if (authorized!!){
                loginFrame!!.visibility = View.GONE
            } else {
                loginFrame!!.visibility = View.VISIBLE
            }
            startViewModel!!.loadObservations()
        })

        startViewModel.hasObservations.observe(this, Observer  { hasObservations ->
            if (!hasObservations!!){
                noRunsHint!!.visibility = View.VISIBLE
            } else {
                noRunsHint!!.visibility = View.GONE
            }
        })
        */

        startViewModel.lastRefresh.observe(this, Observer  { lastRefresh ->
            list.adapter!!.notifyDataSetChanged()
        })

        view.signIn.setOnClickListener {
            securityViewModel.signIn(view.signinEmail.text.toString(), view.signinPassword.text.toString())
        }

        list.layoutManager = LinearLayoutManager(list.context)
        val metrics = DisplayMetrics()
        activity!!.windowManager.getDefaultDisplay().getMetrics(metrics)
        list.adapter = MyTimelineItemRecyclerViewAdapter(TimelineContent.ITEMS, listener, metrics.widthPixels - 20)
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        securityViewModel!!.onActivityResult(requestCode, resultCode, data!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(StartViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTimelineInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnTimelineInteractionListener")
        }
    }

    interface OnTimelineInteractionListener {
        fun onTimelineClick(v:View, item: TimelineContent.TimelineItem?)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_lists, menu)
        this.menu = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionRefresh -> {
                startViewModel.loadObservations()
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }

    companion object {
        private val EMAIL = "email"

        fun newInstance(): StartFragment {
            return StartFragment()
        }
    }

}
