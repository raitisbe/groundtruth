package groundtruth.timeline

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import groundtruth.Config
import groundtruth.Requests
import groundtruth.UserHelpers
import groundtruth.observations.ObservationsContent
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class StartViewModel : ViewModel() {
    var lastRefresh: MutableLiveData<Date> = MutableLiveData()
    var hasObservations: MutableLiveData<Boolean> = MutableLiveData()
    fun loadObservations(){
        val jsonObjectRequest = JsonArrayRequest(Request.Method.GET, Config.url + "observations/list_home", null, Response.Listener { response ->
            try {
                TimelineContent.ITEMS.clear()
                hasObservations.value = response.length() > 0
                for (i in 0..(response.length() - 1)) {
                    val item: JSONObject = response.getJSONObject(i)
                    TimelineContent.ITEMS.add(TimelineContent.TimelineItem(item.getString("_id"),
                            getName(item),
                            item.getString("date"),
                            item.getString("type"),
                            UserHelpers.getUsername(item),
                            UserHelpers.getProfileImage(item),
                            if (item.isNull("screenshot")) "" else item.getString("screenshot")
                    ))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            lastRefresh.value = Date()
        }, Response.ErrorListener {
            lastRefresh.value = Date()
            ObservationsContent.ITEMS.clear()
            hasObservations.value = false
        })

        Requests.queue.add(jsonObjectRequest)

    }

    private fun getName(item: JSONObject) =
            if (!item.isNull("name")) item.getString("name") else ""


}
