package groundtruth

interface LocatableFragmentInteractionListener {
    fun locate()
}