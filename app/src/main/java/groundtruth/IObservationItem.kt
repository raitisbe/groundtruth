package groundtruth

interface IObservationItem {
    val id: String
    val name: String
    val date: String
    val type: String
    val screenshot: String
}