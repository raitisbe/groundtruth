package groundtruth.friends

import android.view.View

interface IPeopleListFragment {
    fun onItemClick(v: View?, item: FriendsItem, position: Int)

}
