package groundtruth.friends

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import kotlinx.android.synthetic.main.search_friends_fragment.view.*
import ground.truth.R
import groundtruth.SecurityViewModel

class SearchFriendsFragment : Fragment(), IPeopleListFragment {
    private var mViewModel: FriendsViewModel? = null
    lateinit var securityViewModel: SecurityViewModel
    lateinit var friendsViewModel: FriendsViewModel
    private lateinit var menu: Menu
    lateinit var list: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.search_friends_fragment, container, false)

        setHasOptionsMenu(true)


        securityViewModel = ViewModelProviders.of(this.activity!!).get(SecurityViewModel::class.java)

        friendsViewModel = ViewModelProviders.of(this.activity!!).get(FriendsViewModel::class.java)

        list = view.findViewById(R.id.list)

        friendsViewModel.lastSearch.observe(this, Observer  { lastSearch ->
            list.adapter!!.notifyDataSetChanged()
        })

        list.layoutManager = LinearLayoutManager(list.context)
        list.adapter = MyFriendsItemRecyclerViewAdapter(PeopleContent.ITEMS, this, context!!.getDrawable(R.color.colorAccent), context!!.getDrawable(R.color.colorSuccess))

        view.filter.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                friendsViewModel.filterPeople(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })


        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        securityViewModel!!.onActivityResult(requestCode, resultCode, data!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(FriendsViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_add_friends, menu)
        this.menu = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.done -> {
                friendsViewModel.addFriends {
                    list.adapter!!.notifyItemChanged(PeopleContent.ITEMS.indexOf(it))
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }

    override fun onItemClick(v: View?, item: FriendsItem, position: Int) {
        item.selected = if(item.selected == 0) 1 else 0
        list.adapter!!.notifyItemChanged(position)
    }

    companion object {
        private val EMAIL = "email"

        fun newInstance(): SearchFriendsFragment {
            return SearchFriendsFragment()
        }
    }

}
