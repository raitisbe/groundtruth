package groundtruth.friends

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import groundtruth.Config
import groundtruth.Requests
import groundtruth.UserHelpers
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class FriendsViewModel : ViewModel() {
    var lastRefresh: MutableLiveData<Date> = MutableLiveData()
    var hasFriends: MutableLiveData<Boolean> = MutableLiveData()
    var lastSearch: MutableLiveData<Date> = MutableLiveData()

    fun loadFriends(){
        val jsonObjectRequest = JsonArrayRequest(Request.Method.GET, Config.url + "friends/list", null, Response.Listener { response ->
            try {
                FriendsContent.ITEMS.clear()
                hasFriends.value = response.length() > 0
                for (i in 0..(response.length() - 1)) {
                    val item: JSONObject = response.getJSONObject(i)
                    FriendsContent.ITEMS.add(FriendsItem(item.getString("_id"),
                            getName(item),
                            UserHelpers.getProfileImage(item), 0
                    ))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            lastRefresh.value = Date()
        }, Response.ErrorListener {
            lastRefresh.value = Date()
            FriendsContent.ITEMS.clear()
            hasFriends.value = false
        })

        Requests.queue.add(jsonObjectRequest)

    }

    private fun getName(item: JSONObject) =
            if (!item.isNull("name")) item.getString("name") else "No name"

    fun filterPeople(filter: String) {
        val jsonObjectRequest = JsonArrayRequest(Request.Method.GET, Config.url + "friends/find/" + filter, null, Response.Listener { response ->
            try {
                PeopleContent.ITEMS.clear()
                for (i in 0..(response.length() - 1)) {
                    val item: JSONObject = response.getJSONObject(i)
                    PeopleContent.ITEMS.add(FriendsItem(item.getString("_id"),
                            getName(item),
                            UserHelpers.getProfileImage(item), 0
                    ))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            lastSearch.value = Date()
        }, Response.ErrorListener {
            lastSearch.value = Date()
            PeopleContent.ITEMS.clear()
        })

        Requests.queue.add(jsonObjectRequest)
    }

    fun addFriends(listener: ((item: FriendsItem) -> Unit)) {
        PeopleContent.ITEMS.forEach {
            if(it.selected == 1) {
                val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, Config.url + "friends/add/" + it.id, null, Response.Listener { response ->
                    try {
                        it.selected = 2
                        listener(it)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener {
                    println(it.message)
                })

                Requests.queue.add(jsonObjectRequest)
            }
        }
    }

}
