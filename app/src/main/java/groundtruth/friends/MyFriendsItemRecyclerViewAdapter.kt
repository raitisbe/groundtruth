package groundtruth.friends

import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.friends_item_fragment.view.*
import ground.truth.R
import groundtruth.IAvatarHolder
import groundtruth.UserHelpers


class MyFriendsItemRecyclerViewAdapter(
        private val mValues: List<FriendsItem>,
        private val mListener: IPeopleListFragment, val selectedColor: Drawable, val addedColor: Drawable)
    : RecyclerView.Adapter<MyFriendsItemRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as FriendsItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onItemClick(v, item, mValues.indexOf(item))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.friends_item_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContentView.text = item.name
        UserHelpers.getProfileImage(holder, item.avatar)
        holder.mHorizontalLayout.background = if(item.selected == 1) selectedColor else if( item.selected == 2) addedColor else null

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView), IAvatarHolder {
        override val avatar: ImageView = mView.avatar
        val mContentView: TextView = mView.user
        val mHorizontalLayout: LinearLayout = mView.horizontalLayout

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
