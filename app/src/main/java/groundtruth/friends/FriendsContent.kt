package groundtruth.friends

import android.os.Parcel
import android.os.Parcelable
import java.util.ArrayList
import java.util.HashMap

object FriendsContent {
    val ITEMS: MutableList<FriendsItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, FriendsItem> = HashMap()

    init {
    }

    private fun addItem(item: FriendsItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }
}

object PeopleContent {
    val ITEMS: MutableList<FriendsItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, FriendsItem> = HashMap()

    init {
    }

    private fun addItem(item: FriendsItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }
}

data class FriendsItem(val id: String, val name: String, val avatar: String, var selected: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt()) {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0!!.writeString(this.id)
        p0!!.writeString(this.name)
        p0!!.writeString(this.avatar)
        p0!!.writeInt(this.selected)
    }

    override fun toString(): String = name
    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FriendsItem> {
        override fun createFromParcel(parcel: Parcel): FriendsItem {
            return FriendsItem(parcel)
        }

        override fun newArray(size: Int): Array<FriendsItem?> {
            return arrayOfNulls(size)
        }
    }
}
