package groundtruth.friends

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import androidx.navigation.Navigation

import kotlinx.android.synthetic.main.friends_fragment.view.*
import ground.truth.R
import groundtruth.SecurityViewModel

class FriendsFragment : Fragment(), IPeopleListFragment {

    private var mViewModel: FriendsViewModel? = null
    lateinit var securityViewModel: SecurityViewModel
    lateinit var friendsViewModel: FriendsViewModel
    private lateinit var menu: Menu

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.friends_fragment, container, false)

        setHasOptionsMenu(true)


        securityViewModel = ViewModelProviders.of(this.activity!!).get(SecurityViewModel::class.java)

        friendsViewModel = ViewModelProviders.of(this.activity!!).get(FriendsViewModel::class.java)

        val list = view.findViewById<RecyclerView>(R.id.list)

        friendsViewModel.hasFriends.observe(this, Observer  { hasFriends ->
            if (!hasFriends!!){
                view.noFriendsHint!!.visibility = View.VISIBLE
            } else {
                view.noFriendsHint!!.visibility = View.GONE
            }
        })

        friendsViewModel.lastRefresh.observe(this, Observer  { lastRefresh ->
            list.adapter!!.notifyDataSetChanged()
        })

        list.layoutManager = LinearLayoutManager(list.context)
        list.adapter = MyFriendsItemRecyclerViewAdapter(FriendsContent.ITEMS, this, context!!.getDrawable(R.color.colorAccent), context!!.getDrawable(R.color.colorSuccess))

        view.addFriends.setOnClickListener{ it ->
            Navigation.findNavController(it).navigate(R.id.action_friendsFragment_to_searchFriendsFragment)
        }

        friendsViewModel.loadFriends()
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        securityViewModel!!.onActivityResult(requestCode, resultCode, data!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(FriendsViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_lists, menu)
        this.menu = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionRefresh -> {
                friendsViewModel.loadFriends()
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }

    override fun onItemClick(v: View?, item: FriendsItem, position: Int) {

    }

    companion object {
        private val EMAIL = "email"

        fun newInstance(): FriendsFragment {
            return FriendsFragment()
        }
    }

}
