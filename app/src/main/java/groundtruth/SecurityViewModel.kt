package groundtruth

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Bitmap
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import groundtruth.Config
import groundtruth.Requests
import groundtruth.StartActivity
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

class SecurityViewModel : ViewModel() {
    private var callbackManager: CallbackManager
    lateinit var activity: StartActivity
    var authorized: MutableLiveData<Boolean> = MutableLiveData()
    var profileName: MutableLiveData<String> = MutableLiveData()
    var profilePic: MutableLiveData<Bitmap> = MutableLiveData()

    init {
        authorized.value = true
        callbackManager = CallbackManager.Factory.create()
    }

    fun start(){
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        signinByFacebook()
                    }

                    override fun onCancel() {
                        // App code
                    }

                    override fun onError(exception: FacebookException) {
                        // App code
                    }
                })
        checkAuthorized(true)
    }

    fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun signinByFacebook() {
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired
        if (!isLoggedIn) {
            authorized.value = false
            return
        }
        //val profile = Profile.getCurrentProfile()

        val parameters = JSONObject()
        parameters.put("access_token", accessToken!!.token)

        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, Config.url + "auth/fbsignin/", parameters, Response.Listener { response ->
            try {
                if (response.getString("result") == "success") {
                    checkAuthorized(false)
                } else {
                    authorized.value = false
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error ->
            authorized.value = false
            Log.e("AUTH", error.toString())
        })

        Requests.queue.add(jsonObjectRequest)
    }

    private fun checkAuthorized(tryOherMethods:Boolean) {
            val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, Config.url + "auth/authorized", null, Response.Listener { response ->
                try {
                    if (response.getBoolean("result")) {
                        authorized.value = true
                        try {
                            val user = response.getJSONObject("user")
                            val name = user.getJSONObject("name")
                            profileName.value = name.getString("first") + " " + name.getString("last")
                            loadAvatarImage(user)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    } else {
                        if(tryOherMethods)
                            signinByFacebook()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener {it: VolleyError ->
                authorized.value = false
                it.printStackTrace()
            })

            Requests.queue.add(jsonObjectRequest)
        }


        @Throws(JSONException::class)
        private fun loadAvatarImage(user: JSONObject) {
            try {
                var img_url = user.getJSONObject("services").getJSONObject("facebook").getString("avatar")
                img_url = img_url.replace("600".toRegex(), "64")
                println(img_url)
                val request = ImageRequest(img_url,
                        Response.Listener { bitmap -> profilePic.value = bitmap }, 64, 64, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.ARGB_8888,
                        Response.ErrorListener { })
                Requests.fbqueue.add(request)
            } catch(ex:Exception){

            }
        }

    fun logout() {
        LoginManager.getInstance().logOut()
        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, Config.url + "auth/signout", null, Response.Listener { response ->
            try {
                if (response.getBoolean("result")) {
                    authorized.value = false
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener {it: VolleyError ->
            it.printStackTrace()
        })

        Requests.queue.add(jsonObjectRequest)

    }

    fun signIn(signinEmail: String, signinPassword: String) {
        val parameters = JSONObject()
        parameters.put("email", signinEmail)
        parameters.put("password", signinPassword)

        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, Config.url + "auth/signin", parameters, Response.Listener { response ->
            try {
                if (response.getBoolean("result")) {
                    checkAuthorized(false)
                } else {
                    Toast.makeText(activity, "Authorization unsuccessful", Toast.LENGTH_SHORT).show()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { it: VolleyError ->
            it.printStackTrace()
        })

        Requests.queue.add(jsonObjectRequest)
    }

    companion object{
        fun create(activity: StartActivity): SecurityViewModel {
            var model = ViewModelProviders.of(activity).get(SecurityViewModel::class.java)
            model.activity = activity
            return model
        }
    }
}